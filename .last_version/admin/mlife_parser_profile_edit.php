<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

CModule::IncludeModule("mlife.parser");
use Bitrix\Main\Localization\Loc;
use Mlife\Parser;
Loc::loadMessages(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("mlife.parser");

if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$error = array();
	
$classError = false;
	
if($parser = trim($_REQUEST["parser"])){
	$classOptions = "\\Mlife\\Parser\\Options\\".strtolower($parser)."\\Option";
	if(!class_exists($classOptions)) {
		$error[] = $classOptions." not exists";
		$classError = true;
	}else{
		$className = $classOptions::CLASS_PROFILE;
	}
	$classOptions = new $classOptions;
}else{
	$error[] = "class not exists";
	$classError = true;
}

$str_CATEGORY = "";
$str_IBLOCK = "";

if(!$classError){
	
	CModule::IncludeModule("iblock");
	$arIblock = array();
	$res = CIBlock::GetList(
		Array(), 
		Array(
			'ACTIVE'=>'Y',
		), true
	);
	while($ar_res = $res->Fetch())
	{
		$i++;
		$arIblock[$ar_res['ID']] = "[".$ar_res['IBLOCK_TYPE_ID']."] [".$ar_res['CODE']."] ".$ar_res['NAME'];
	}
	
	$iblockId = 0;
	if(intval($_REQUEST["iblock"])>0){
		$iblockId = $_REQUEST["iblock"];
	}elseif(intval($_REQUEST["iblockid"])>0) {
		$iblockId = $_REQUEST["iblockid"];
	}
	
	$ID = intval($_REQUEST['ID']);
	$arParams = array();
	
	if($REQUEST_METHOD == "POST" && ($save!="" || $apply!="") && $POST_RIGHT=="W" && check_bitrix_sessid() && $iblockId>0){
		
		$str_IBLOCK = $iblockId;
		$str_CATEGORY = (intval($_REQUEST["category"])>0) ? intval($_REQUEST["category"]) : null;
		foreach($_REQUEST as $key=>$val){
			if(substr($key,0,6)=="PARAM_") $arParams[$key] = $val;
		}
		//print_r($arParams);die();
		$str_PARAMS = serialize($arParams);
		
		$arFields = array(
			"IBLOCK" => $str_IBLOCK,
			"CATEGORY" => $str_CATEGORY,
			"PARAMS" => $str_PARAMS,
		);
		
		$noupdate = false;
		
		if($ID > 0){
			$res = $className::update($ID,$arFields);
		}
		else{
			$chech = $className::getList(array(
				'select' => array("ID"),
				'filter' => array("IBLOCK"=>$str_IBLOCK,"CATEGORY"=>($str_CATEGORY==null) ? false : $str_CATEGORY),
			));
			if($chech->Fetch()){
				$error[] = Loc::getMessage("MLIFE_PARSER_PROFILE_EDIT_ERR1");
				$noupdate = true;
			}else{
				$res = $className::add($arFields);
			}
		}
		
		if(!$noupdate){
			if($res->isSuccess()){
				if ($_REQUEST['apply'] != "" && $ID>0){
					LocalRedirect("mlife_parser_profile_edit.php?ID=".$ID."&mess=ok&lang=".LANG."&parser=".$parser);
				}
				elseif ($_REQUEST['apply'] != ""){
					LocalRedirect("mlife_parser_profile_edit.php?ID=".$res->getId()."&mess=ok&lang=".LANG."&parser=".$parser);
				}
				else{
					LocalRedirect("mlife_parser_profile.php?lang=".LANG."&parser=".$parser);
				}
			}
		}
		
	}
	
	$aTabs = array(
		array("DIV" => "edit1", "TAB" => Loc::getMessage("MLIFE_PARSER_PROFILE_EDIT_TAB1"), "ICON" => "vote_settings1", "TITLE" => Loc::getMessage("MLIFE_PARSER_PROFILE_EDIT_TAB1")),
	);
	
	if($ID > 0){
		$res = $className::getById($ID)->Fetch();
		if(!$res) {
			$error[] = Loc::getMessage("MLIFE_PARSER_PROFILE_EDIT_ERR2");
		}else{
			$str_IBLOCK = $res["IBLOCK"];
			$str_CATEGORY = $res["CATEGORY"];
			$arParams = unserialize($res["PARAMS"]);
			$iblockId = $str_IBLOCK;
		}
	}
	
	$arCategory = array();
	
	if($iblockId>0) {
		
		$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),array('IBLOCK_ID' => $iblockId));
		while ($arSect = $rsSect->GetNext()){
		  $arCategory[$arSect["ID"]] = str_replace(array(1,2,3,4,5),array("-","--","---","----","-----"),$arSect["DEPTH_LEVEL"]).' ['.$arSect["ID"].'] - '.$arSect["NAME"];
		}
		
	}
	
	if($iblockId>0) {
	$aTabs[] = array("DIV" => "edit2", "TAB" => Loc::getMessage("MLIFE_PARSER_PROFILE_EDIT_TAB2"), "ICON" => "vote_settings2", "TITLE" => Loc::getMessage("MLIFE_PARSER_PROFILE_EDIT_TAB2"));
	}
	$tabControl = new CAdminTabControl("tabControl", $aTabs,false,true);
	
}
	
$APPLICATION->SetTitle(Loc::getMessage("MLIFE_PARSER_PROFILE_EDIT_TITLE"));
	
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

if(count($error)>0){
	CAdminMessage::ShowMessage(implode(', ',$error));
}

?>
<?
if(!$classError){?>
	<form method="POST" Action="<?echo $APPLICATION->GetCurPage()?>?parser=<?=$parser?>" ENCTYPE="multipart/form-data" name="post_form">
	<?echo bitrix_sessid_post();?>
	<input type="hidden" name="lang" value="<?=LANG?>">
	<input type="hidden" name="ID" value="<?=$ID?>">
	<?
	$tabControl->Begin();
	?>
	<?
	$tabControl->BeginNextTab();
	?>
	<tr>
		<td width="40%"><?=Loc::getMessage("MLIFE_PARSER_PROFILE_EDIT_TITLE1")?></td>
		<td width="60%">
			<select name="iblockid" onChange="location = '<?echo $APPLICATION->GetCurPage()?>?parser=<?=$parser?>&iblock='+this.value;">
				<option value="0"><?=Loc::getMessage("MLIFE_PARSER_PROFILE_EDIT_TITLE2")?></option>
				<?foreach($arIblock as $ibId=>$ibname){?>
				<option value="<?=$ibId?>" <?if($iblockId==$ibId){?> selected="selected"<?}?>><?=$ibname?></option>
				<?}?>
			</select>
		</td>
	</tr>
	<tr>
		<td width="40%"><?=Loc::getMessage("MLIFE_PARSER_PROFILE_EDIT_TITLE3")?></td>
		<td width="60%">
			<select name="category">
				<option value="0"<?if($str_CATEGORY==null){?> selected="selected"<?}?>><?=Loc::getMessage("MLIFE_PARSER_PROFILE_EDIT_TITLE4")?></option>
				<?foreach($arCategory as $ibId=>$ibname){?>
				<option value="<?=$ibId?>"<?if($str_CATEGORY==$ibId){?> selected="selected"<?}?>><?=$ibname?></option>
				<?}?>
			</select>
		</td>
	</tr>
	<?if($iblockId>0) {?>
		<?
		$tabControl->BeginNextTab();
		?>
		<?=$classOptions->getParamsHtml($arParams,$iblockId);?>
	<?}?>
	<?
	$tabControl->Buttons(
	  array(
		"disabled"=>($POST_RIGHT<"W"),
		"back_url"=>"mlife_parser_profile.php?lang=".LANG."&parser=".$parser,
	  )
	);
	?>
	<?
	$tabControl->End();
	?>
	<?
	$tabControl->ShowWarnings("post_form", $message);
	?>
<?}?>
<?echo BeginNote();?>
<?echo $classOptions->getNote()?>
<?echo EndNote();?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>