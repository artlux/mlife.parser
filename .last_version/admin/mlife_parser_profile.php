<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

CModule::IncludeModule("mlife.parser");
use Bitrix\Main\Localization\Loc;
use Mlife\Parser;
Loc::loadMessages(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("mlife.parser");

if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$error = array();
	
$classError = false;
	
if($parser = trim($_REQUEST["parser"])){
	$classOptions = "\\Mlife\\Parser\\Options\\".strtolower($parser)."\\Option";
	if(!class_exists($classOptions)) {
		$error[] = $classOptions." not exists";
		$classError = true;
	}else{
		$className = $classOptions::CLASS_PROFILE;
	}
}else{
	$error[] = "class not exists";
	$classError = true;
}

if(!$classError){
	
	$listTableId = "tbl_mlife_parser_profile_".$parser;

	$oSort = new CAdminSorting($listTableId, "ID", "ASC");
	$arOrder = (strtoupper($by) === "ID"? array($by => $order): array($by => $order, "ID" => "ASC"));
	
	$adminList = new CAdminList($listTableId, $oSort);
	
	// ��������� ��������� � ��������� ��������
	if(($arID = $adminList->GroupAction()) && $POST_RIGHT=="W")
	{
		if($_REQUEST['action_target']=='selected')
		{
			$rsData = $className::getList(
				array(
					'order' => $arOrder,
					'select' => array('ID'),
				)
			);
			while($arRes = $rsData->Fetch())
			  $arID[] = $arRes['ID'];
		}
		
		if($_REQUEST['action']=="delete") {
			foreach($arID as $ID)
			{
				if(strlen($ID)<=0)
					continue;
					$ID = IntVal($ID);
					
				$res = $className::delete(array("ID"=>$ID));
			}
		}
		
	}
	
	$Pars = $className::getList(
		array(
			'order' => $arOrder,
		)
	);
	
	$Pars = new CAdminResult($Pars, $listTableId);
	$Pars->NavStart();

	$adminList->NavText($Pars->GetNavPrint(Loc::getMessage("MLIFE_PARSER_PROFILELIST_NAV")));

	$cols = $className::getMap();
	$colHeaders = array();

	foreach ($cols as $colId => $col)
	{
		$tmpAr = array(
			"id" => $colId,
			"content" => $col["title"],
			"sort" => $colId,
			"default" => true,
		);
		$colHeaders[] = $tmpAr;
	}
	$adminList->AddHeaders($colHeaders);

	$visibleHeaderColumns = $adminList->GetVisibleHeaderColumns();
	
	while ($arRes = $Pars->GetNext())
	{
		$row =& $adminList->AddRow($arRes["ID"], $arRes);
		$arActions = $classOptions::getCtMenu($arRes["ID"],LANG,$parser);
		foreach($arActions as &$act){
			$act["ACTION"] = $adminList->ActionRedirect($act["ACTION"]);
		}
		$row->AddActions($arActions);
	}
	
	// actions buttins
	$adminList->AddGroupActionTable(array(
		"delete" => Loc::getMessage("MLIFE_PARSER_PROFILELIST_MENU_DELETE"),
	));

	$adminList->AddFooter(
		array(
			array(
				"title" => Loc::getMessage("MAIN_ADMIN_LIST_SELECTED"),
				"value" => $Pars->SelectedRowsCount()
			),
			array(
				"counter" => true,
				"title" => Loc::getMessage("MAIN_ADMIN_LIST_CHECKED"),
				"value" => "0"
			),
		)
	);

	//������ �� ������
	$aContext = array(
	  array(
		"TEXT"=>Loc::getMessage("MLIFE_PARSER_PROFILELIST_MENU_ADD"),
		"LINK"=>'mlife_parser_profile_edit.php?lang='.LANG."&parser=".$parser,
		"TITLE"=>Loc::getMessage("MLIFE_PARSER_PROFILELIST_MENU_ADD"),
		"ICON"=>"btn_new",
	  ),
	);

	$adminList->AddAdminContextMenu($aContext);

	$adminList->CheckListMode();

	$APPLICATION->SetTitle(Loc::getMessage("MLIFE_PARSER_PROFILELIST_TITLE"));
	
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
if(count($error)>0){
	CAdminMessage::ShowMessage(implode(', ',$error));
}

?>
<?
if(!$classError){
$adminList->DisplayList();
}
?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>