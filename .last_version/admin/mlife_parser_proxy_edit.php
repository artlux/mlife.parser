<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.proxy
 * @copyright  2014 Zahalski Andrew
 */

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

CModule::IncludeModule("mlife.parser");
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("mlife.parser");

if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

?>
<?
$aTabs = array(
  array("DIV" => "edit1", "TAB" => Loc::getMessage("MLIFE_PARSER_PROXYEL_PARAM"), "ICON"=>"main_user_edit", "TITLE"=>Loc::getMessage("MLIFE_PARSER_PROXYEL_PARAM")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);


if($REQUEST_METHOD == "POST" && ($save!="" || $apply!="") && $POST_RIGHT=="W" && check_bitrix_sessid()){
  
	$proxyAr = array();
	$proxyAr = explode(PHP_EOL,$_REQUEST["PROXY"]);
	foreach($proxyAr as $prx){
		$prx = trim($prx);
		if(strlen($prx)<25){
			$baseCheck = \Mlife\Parser\ProxyTable::getList(
				array(
					'select' => array('ID'),
					'filter' => array("PROXY"=>$prx),
					'limit' => 1,
				)
			);
			
			if(!$baseCheck->Fetch()){
				\Mlife\Parser\ProxyTable::add(array("PROXY"=>$prx));
			}
		}
	}
	LocalRedirect("mlife_parser_proxy.php?lang=".LANG);
}

$APPLICATION->SetTitle(($ID>0? Loc::getMessage("MLIFE_PARSER_PROXYEL_EDIT").$ID : Loc::getMessage("MLIFE_PARSER_PROXYEL_ADD")));

?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<form method="POST" Action="<?echo $APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data" name="post_form">
<?echo bitrix_sessid_post();?>
<input type="hidden" name="lang" value="<?=LANG?>">
<input type="hidden" name="ID" value="<?=$ID?>">
<?
$tabControl->Begin();
?>
<?
$tabControl->BeginNextTab();
?>
	<tr>
		<td width="40%"><?=Loc::getMessage("MLIFE_PARSER_PROXYEL_PARAM_NAME")?></td>
		<td width="60%">
			<textarea name="PROXY"></textarea>
		</td>
	</tr>
<?
$tabControl->Buttons(
  array(
    "disabled"=>($POST_RIGHT<"W"),
    "back_url"=>"mlife_parser_proxy.php?lang=".LANG,
    
  )
);
?>
<input type="hidden" name="lang" value="<?=LANG?>">
<?
$tabControl->End();
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>