<?
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser\Options\yandex;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Parser extends \Mlife\Parser\Parser {
	
	public function startParser($profileId){
		
		self::$profileId = $profileId;
		
		if(intval($_REQUEST["elid"]) > 0) {
			self::$arFilterId = intval($_REQUEST["elid"]);
		}
		
		if(!$_REQUEST["option"] || $_REQUEST["option"]=="get_count"){
		
			$count = self::getCount($profileId);
			
			if($count>0) {
				
				$this->getMess(false,$profileId,array("mess"=>sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR4"),$count),"option"=>"get_url"),true);
			
			}else{
				
				$this->getMess("001",$profileId,array(),false);
			
			}
			
		}elseif($_REQUEST["option"]=="get_url"){
			
			$urlList = self::getUrl($profileId);
			//print_r($urlList);die();

			if(!$urlList) {
				$this->getMess("002",$profileId,array(),false);
			}else{
				
				foreach($urlList as $link){
					
					$contentOb = new \Mlife\Parser\ContentCurl();
					
					$proxy = false;
					
					if(\COption::GetOptionString("mlife.parser", "proxyotp", "N")=="Y"){
					
						$proxer = \Mlife\Parser\ProxyTable::getProxy();
						
						if(isset($proxer["PROXY"])) $proxy = $proxer["PROXY"];
					
					}
					
					if(self::$arFilterId && \COption::GetOptionString("mlife.parser", "proxyotpel", "N")=="Y") {
						$proxy = false;
					}else{
						if(!$proxy && \COption::GetOptionString("mlife.parser", "proxyotp", "N")=="Y") {
							$this->getMess("005",$profileId,array(),false);
							die();
						}
					}
					
					if($proxy){
					
						$contentOb->n_proxy = 1;
						$contentOb->use_proxy_list = true;
						$contentOb->array_proxy = array($proxy);
					
					}
					
					$contentOb->get($link["URL"],null,null,$link);
					$result = $contentOb->execute();
					$func = $link["FUNC_PARSE"]; //������ ������� ��� ��������� ����������
					call_user_func_array(array($this,$func),$result);
					
					break;
				}
				
			}
			
		}elseif($_REQUEST["option"]=="get_capcha"){
			
			$urlList = self::getUrl($profileId);
			
			if(!$urlList) {
				$this->getMess("002",$profileId,array(),false);
			}else{
				
				foreach($urlList as $link){
					
					$contentOb = new \Mlife\Parser\ContentCurl();
					
					$proxy = false;
					
					if(\COption::GetOptionString("mlife.yandexload", "proxyotp", "N")=="Y"){
					
						$proxer = \Mlife\Parser\ProxyTable::getProxy();
						//print_r($proxer);die();
						if(isset($proxer["PROXY"])) $proxy = $proxer["PROXY"];
						
						
					
					}
					if($_REQUEST['lastproxy']) $proxy = $_REQUEST['lastproxy'];
					
					if(self::$arFilterId && \COption::GetOptionString("mlife.parser", "proxyotpel", "N")=="Y") {
						$proxy = false;
					}else{
						if(!$proxy && \COption::GetOptionString("mlife.yandexload", "proxyotp", "N")=="Y") {
							$this->getMess("005",$profileId,array(),false);
							die();
						}
					}
					
					if($proxy){
					
						$contentOb->n_proxy = 1;
						$contentOb->use_proxy_list = true;
						$contentOb->array_proxy = array($proxy);
					
					}
					//print_r($proxy);
					
					$link["URL"] = "http://market.".\COption::GetOptionString("mlife.parser","yandex_show_capcha_url","yandex.ru")."/checkcaptcha?retpath=".urlencode($_REQUEST["retpath"])."&key=".urlencode($_REQUEST["key"])."&rep=".urlencode($_REQUEST["rep"]);
					$link["FUNC_PARSE"] = $_REQUEST["func"];
					$link["ELEMENT_ID"] = $_REQUEST["els"];
					if($_REQUEST['lastproxy']) $link["PROXY"] = $_REQUEST['lastproxy'];
					//print_r($link);die();
					//echo'<pre>';print_r($link);echo'</pre>';
					$contentOb->get($link["URL"],null,null,$link);
					$result = $contentOb->execute();
					$func = $link["FUNC_PARSE"]; //������ ������� ��� ��������� ����������
					call_user_func_array(array($this,$func),$result);
					
					break;
				}
			}
			
		}
		
	}
	
	public function getUrl($profileId){
		
		$arParams = self::getProfileParams($profileId);
		if(!$arParams) {
			$this->getMess("003",$profileId,array(),false);
		}else{
			
			\CModule::IncludeModule("iblock");
			$arFilter = array();
			$arFilter["IBLOCK_ID"] = $arParams["IBLOCK"];
			if(intval($arParams["CATEGORY"])>0) {
				$arFilter["SECTION_ID"] = $arParams["CATEGORY"];
				$arFilter["INCLUDE_SUBSECTIONS"] = "Y";
			}
			$image = false;
			$arFilter[] = array("LOGIC"=>"AND", array("!PROPERTY_YANDEX_ID" => false), array("!PROPERTY_YANDEX_ID" => 'no'));
			if($arParams["PARAM_EL_DETAIL_PICTURE"][1] || $arParams["PARAM_EL_PREVIEW_PICTURE"][1]){
				$arFilter[] = array("LOGIC"=>"OR",array("!PROPERTY_YA_ACTIVE_HAR_VALUE" => "Y"), array("!PROPERTY_YA_ACTIVE_IMG_VALUE" => "Y"));
				$image = true;
			}else{
				$arFilter[] = array("!PROPERTY_YA_ACTIVE_HAR_VALUE" => "Y");
			}
			if(self::$arFilterId) $arFilter['ID'] = self::$arFilterId;
			$arLinks = array();
			$res = \CIBlockElement::GetList(array("rand"=>"asc"),$arFilter,false,Array("nTopCount"=>self::$limitLink),array("ID","PROPERTY_YANDEX_ID","PROPERTY_YA_ACTIVE_HAR",
			"PROPERTY_YA_ACTIVE_IMG"));
			while($ar = $res->GetNext(false,false)){
				if(strpos($ar["PROPERTY_YANDEX_ID_VALUE"],"yandex") !== false){
					$ar["PROPERTY_YANDEX_ID_VALUE"] = preg_replace("/.*product\/([0-9]+)\??.*/is","$1",$ar["PROPERTY_YANDEX_ID_VALUE"]);
				}
				if(!$ar["PROPERTY_YA_ACTIVE_IMG_VALUE"]){
					$arLinks[] = array("URL"=>"https://market.yandex.ru/product/".$ar["PROPERTY_YANDEX_ID_VALUE"]."?track=tabs&lr=213","ELEMENT_ID"=>$ar["ID"],
					"FUNC_PARSE"=>"contentImage");
				}
				if($image){
					if(!$ar["PROPERTY_YA_ACTIVE_HAR_VALUE"]){
						$arLinks[] = array("URL"=>"https://market.yandex.ru/product/".$ar["PROPERTY_YANDEX_ID_VALUE"]."/spec?track=tabs&lr=213","ELEMENT_ID"=>$ar["ID"],
						"FUNC_PARSE"=>"contentHar");
					}
				}
			}
			
			if(count($arLinks)==0) return false;
			return $arLinks;
			
		}
		
	}
	
	//��������������
	public function contentHar($response, $info, $request){
		
		$arParams = self::$arParams;
		$profileId = self::$profileId;
		
		
		if($info['http_code']!==200)
		{
			//������� ������
			if($request["proxy"]){
				\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
			}
			
			if($info['http_code'] == 404){
				
				\CModule::IncludeModule('iblock');
				
				$arLoadprops = array();
				$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_HAR", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
				if($ar_enum_list = $db_enum_list->GetNext()){
					$arLoadprops['YA_ACTIVE_HAR'] = $ar_enum_list["ID"];
					\CIBlockElement::SetPropertyValuesEx($request[5]['ELEMENT_ID'], $arParams["IBLOCK"], $arLoadprops);
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR5"),$request["proxy"],$request[5]['ELEMENT_ID'],$info['http_code']);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
				
			}else{
			
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR6"),$request["proxy"],$info['http_code']);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_url"),true,$request['url']);
			
			}
			
		}else{
			
			if(ToLower(SITE_CHARSET) != 'utf-8'){
				$response = $GLOBALS["APPLICATION"]->ConvertCharset($response, "UTF-8", SITE_CHARSET);
			}
			$saw = new \Mlife\Parser\nokogiri('<meta http-equiv="content-type" content="text/html; charset='.SITE_CHARSET.'">'.$response);
			//print_r($response);
			if($prop = $saw->get('img.form__captcha')->toArray()) {
				
				if(\COption::GetOptionString("mlife.parser","yandex_show_capcha","Y")=="Y" /*&& self::$arFilterId*/){
				$defUrl = preg_replace("/(.*)market\.([^\/]+)\/(.*)/","$2",$request[0]);
				\COption::SetOptionString("mlife.parser","yandex_show_capcha_url",$defUrl);
				$html = $saw->get('.form__inner')->toXml();
				global $APPLICATION;
				$proxer = \Mlife\Parser\ProxyTable::getProxy();
				if($request[5]['PROXY']) $proxer = array("PROXY"=>$request[5]['PROXY']);
				if($request["proxy"]) $proxer = array("PROXY"=>$request["proxy"]);
				$html = str_replace('action="/checkcaptcha"','action="'.$APPLICATION->GetCurPageParam("option=get_capcha&func=contentHar&parser=yandex&els=".$request[5]['ELEMENT_ID']."&lastproxy=".$proxer["PROXY"],array("option","parser","els","lastproxy")).'"',$html);
				$html = str_replace("\"get","\"POST",$html);

				$html = preg_replace("/(.*<root>)/is","",$html);
				$html = str_replace('<\/root>',"",$html);
				
				$img = $saw->get("img.form__captcha")->toArray();
				
				$img = $img[0]["src"];
				
					$contentOb = new \Mlife\Parser\ContentCurl();
					//$proxy = false;
					if(\COption::GetOptionString("mlife.yandexload", "proxyotp", "N")=="Y"){
						
						if(isset($proxer["PROXY"])) $proxy = $proxer["PROXY"];
					}
					if(self::$arFilterId && \COption::GetOptionString("mlife.parser", "proxyotpel", "N")=="Y") {
						//$proxy = false;
					}else{
						if(!$proxy && \COption::GetOptionString("mlife.yandexload", "proxyotp", "N")=="Y") {
							$this->getMess("005",$profileId,array(),false);
							die();
						}
					}
					if($proxer["PROXY"]){
						$contentOb->n_proxy = 1;
						$contentOb->use_proxy_list = true;
						$contentOb->array_proxy = array($proxer["PROXY"]);
					}
					//print_r($contentOb->array_proxy);
					//print_r($proxy);
					$contentOb->get($img,null,null,array("PROXY" => $proxer["PROXY"]));
					$result = $contentOb->execute();
					
					if(!$result[0] && $result[1]['http_code']=='302'){
						$contentOb = new \Mlife\Parser\ContentCurl();
						if($proxer["PROXY"]){
							$contentOb->n_proxy = 1;
							$contentOb->use_proxy_list = true;
							$contentOb->array_proxy = array($proxer["PROXY"]);
						}
						$contentOb->get($result[1]['url'],null,null,array("PROXY" => $proxer["PROXY"]));
						$result = $contentOb->execute();
					}
					$tmp_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp274s_yandex_capcha.jpg";
					file_put_contents($tmp_name,$result[0]);
					
					if(!$result[0]) {
						$tmp_name = $img;
						
						$contentOb = new \Mlife\Parser\ContentCurl();
						if($proxer["PROXY"]){
							$contentOb->n_proxy = 0;
							$contentOb->use_proxy_list = false;
							$contentOb->array_proxy = array();
						}
						$contentOb->get($result[1]['url'],null,null,array("PROXY" => $proxer["PROXY"]));
						$result = $contentOb->execute();
						if($result[0]){
							$tmp_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp274s_yandex_capcha.jpg";
							file_put_contents($tmp_name,$result[0]);
						}
						
						
					}
					
					$html = '<img src="'.str_replace($_SERVER["DOCUMENT_ROOT"],"",$tmp_name).'"><br><br>'.preg_replace("/(<img[^>]+>)/is","",$html);
					$html .= '<style>.b-link_captcha_reload, .form__audio {display:none;}</style>';
					$html .= '<script type="text/javascript" src="/bitrix/js/main/jquery/jquery-1.8.3.min.js?146720357193636"></script>';
					$html .= '
					<script>
					var keyId = "";
					$(document).ready(function(){
						$.ajax({
							url: "/bitrix/tools/mlife_parser.php?key='.\COption::GetOptionString("mlife.parser", "antigate_key", "N").'",
							data: {},
							dataType : "html",
							success: function (data, textStatus) {
								if(data!="FALSE"){
									keyId = data;
									setTimeout(function(){
										
										get_capcha();
										
									},15000);
								}else{
								$("button.form__submit").click();
								}
							}
						});
					});
					
					
					function get_capcha(){
						
						$.ajax({
							url: "/bitrix/tools/mlife_parser.php?key='.\COption::GetOptionString("mlife.parser", "antigate_key", "N").'&id="+keyId,
							data: {},
							dataType : "html",
							success: function (data, textStatus) {
								if(data=="FALSE"){
									console.log("return false");
									$("button.form__submit").click();
								}else if(data=="CAPCHA_NOT_READY"){
									setTimeout(function(){
										get_capcha();
									},10000);
								}else{
									$("#rep").val(data);
									$("button.form__submit").click();
								}
							}
						});
						
					}
					
					</script>
					';
					echo $html;die();
				
				}else{
					
					//������� ������
					if($request["proxy"]){
						\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
					}
					
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR7"),$request["proxy"],$info['http_code']);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_url"),true,$request['url']);
				
				//echo'<pre>';print_r($saw->get('.b-captcha'));echo'</pre>';
			}
			
			elseif($prop = $saw->get('.product-spec-wrap__body')->toArray()){
				
				
				
				if(is_array($prop)){
					$g = 0;
					foreach($prop as $group){
						$g++;
						$groupValue[$g] = $group['h2'][0]['#text'];
						if(ToLower(SITE_CHARSET) != 'utf-8') $groupValue[$g] = $GLOBALS["APPLICATION"]->ConvertCharset($groupValue[$g], "UTF-8", SITE_CHARSET);
						$i = 1;
						if($g==1){
							$manufAr = $saw->get("ul.breadcrumbs li")->toArray();
							foreach($manufAr as $mn){
								if(strpos($mn['a']['href'],'guru.xml')!==false){
									$manuf = $mn['a']['span'][0]['#text'];
									$propValue[$g][$i]['name'] = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_MANUF");
									if(ToLower(SITE_CHARSET) != 'utf-8'){
										$propValue[$g][$i]['val'] = $GLOBALS["APPLICATION"]->ConvertCharset($manuf, "UTF-8", SITE_CHARSET);
									}else{
										$propValue[$g][$i]['val'] = $manuf;
									}
									break;
								}
								//echo'<pre>';print_r($mn['a']['href']);echo'</pre>';
							}
							if(!$manufAr){
							$manufAr = $saw->get(".n-breadcrumbs li")->toArray();
							foreach($manufAr as $mn){
								if(strpos($mn['a']['href'],'guru.xml')!==false || $mn['id']=='n-breadcrumbs-brand'){
									$manuf = $mn['a']['span'][0]['#text'];
									$propValue[$g][$i]['name'] = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_MANUF");
									if(ToLower(SITE_CHARSET) != 'utf-8'){
										$propValue[$g][$i]['val'] = $GLOBALS["APPLICATION"]->ConvertCharset($manuf, "UTF-8", SITE_CHARSET);
									}else{
										$propValue[$g][$i]['val'] = $manuf;
									}
									break;
								}
								//echo'<pre>';print_r($mn['a']['href']);echo'</pre>';
							}
							}
							
							//echo'<pre>';print_r($manufAr);echo'</pre>';
							$i++;
						}
						foreach($group['dl'] as $propDetail){
							if(!is_array($propDetail)) continue;
							//echo'<pre>';print_r($propDetail);echo'</pre>';
							
							//die();
							$propValue[$g][$i]['name'] = is_array($propDetail['dt'][0]['span']['#text'][0]) ? $propDetail['dt'][0]['span']['#text'][0] : $propDetail['dt'][0]['span']['#text'];
							if(is_array($propValue[$g][$i]['name']) && isset($propValue[$g][$i]['name'][0])) $propValue[$g][$i]['name'] = $propValue[$g][$i]['name'][0];
							
							$propValue[$g][$i]['name'] = preg_replace('/ {2,}/',' ',$propValue[$g][$i]['name']);
							if(ToLower(SITE_CHARSET) != 'utf-8') $propValue[$g][$i]['name'] = $GLOBALS["APPLICATION"]->ConvertCharset($propValue[$g][$i]['name'], "UTF-8", SITE_CHARSET);
							$propValue[$g][$i]['val'] = is_array($propDetail['dd'][0]['span']['#text']) ? $propDetail['dd'][0]['span']['#text'][0] : $propDetail['dd'][0]['span']['#text'];
							if(ToLower(SITE_CHARSET) != 'utf-8') $propValue[$g][$i]['val'] = $GLOBALS["APPLICATION"]->ConvertCharset($propValue[$g][$i]['val'], "UTF-8", SITE_CHARSET);
							$i++;
						}
					}
				}
				//echo'<pre>';print_r(array($propValue,$groupValue));echo'</pre>';
				
				if(is_array($prop[0]['tbody']['tr']) && count($prop[0]['tbody']['tr']>0)){
					$g = 0;
					foreach($prop[0]['tbody']['tr'] as $val){
						//echo'<pre>';print_r($val);echo'</pre>';
						if(isset($val['th']['class']) && ($val['th']['class']=='b-properties__title')){
							$g++;
							$groupValue[$g] = $val['th']['#text'];
							if(ToLower(SITE_CHARSET) != 'utf-8') $groupValue[$g] = $GLOBALS["APPLICATION"]->ConvertCharset($groupValue[$g], "UTF-8", SITE_CHARSET);
							$i = 1;
							if($g==1){
								$manufAr = $saw->get("#guru")->toArray();
								$manuf = $manufAr[0]['a']['span']['#text'];
								$propValue[$g][$i]['name'] = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_MANUF");
								if(ToLower(SITE_CHARSET) != 'utf-8'){
									$propValue[$g][$i]['val'] = $GLOBALS["APPLICATION"]->ConvertCharset($manuf, "UTF-8", SITE_CHARSET);
								}else{
									$propValue[$g][$i]['val'] = $manuf;
								}
								$i++;
							}
							
						}elseif(is_array($val['th'][0]) && $val['th'][0]['class']=='b-properties__label b-properties__label-title'){
							$propValue[$g][$i]['name'] = $val['th'][0]['span']['#text'];
							if(ToLower(SITE_CHARSET) != 'utf-8') $propValue[$g][$i]['name'] = $GLOBALS["APPLICATION"]->ConvertCharset($propValue[$g][$i]['name'], "UTF-8", SITE_CHARSET);
							$propValue[$g][$i]['name'] = preg_replace('/ {2,}/',' ',$propValue[$g][$i]['name']);
							$propValue[$g][$i]['val'] = $val['td'][0]['#text'];
							if(ToLower(SITE_CHARSET) != 'utf-8') $propValue[$g][$i]['val'] = $GLOBALS["APPLICATION"]->ConvertCharset($propValue[$g][$i]['val'], "UTF-8", SITE_CHARSET);
							$i++;
						}
					}
					
				}
				//die();
				$resa = $this->loadProductHar($propValue,$groupValue,$request[5]['ELEMENT_ID']);
				if($resa) {

					$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR8"),$request[5]['ELEMENT_ID']);
					$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
					
				}else{
					
					\CModule::IncludeModule('iblock');
				
					$arLoadprops = array();
					$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_HAR", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
					if($ar_enum_list = $db_enum_list->GetNext()){
						$arLoadprops['YA_ACTIVE_HAR'] = $ar_enum_list["ID"];
						\CIBlockElement::SetPropertyValuesEx($request[5]['ELEMENT_ID'], $arParams["IBLOCK"], $arLoadprops);
					}
					
					$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR9"),$request[5]['ELEMENT_ID']);
					$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
					
				}
				
			}
			
			else{
			
				if($request["proxy"]){
					\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
				}
				
				$arLoadprops = array();
				$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_HAR", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
				if($ar_enum_list = $db_enum_list->GetNext()){
					$arLoadprops['YA_ACTIVE_HAR'] = $ar_enum_list["ID"];
					\CIBlockElement::SetPropertyValuesEx($request[5]['ELEMENT_ID'], $arParams["IBLOCK"], $arLoadprops);
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR10"),$request[5]['ELEMENT_ID'],$request["proxy"]);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
				
			}
			
		}
		
	}
	
	//�����������
	public function contentImage($response, $info, $request){
		//echo'<pre>';print_r($response);echo'</pre>';
		//print_r($request[5]);die();
		$arParams = self::$arParams;
		$profileId = self::$profileId;
		
		
		if($info['http_code']!==200)
		{
			//������� ������
			if($request["proxy"]){
				\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
			}
			
			if($info['http_code'] == 404){
				
				\CModule::IncludeModule('iblock');
				
				$arLoadprops = array();
				$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_IMG", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
				if($ar_enum_list = $db_enum_list->GetNext()){
					$arLoadprops['YA_ACTIVE_IMG'] = $ar_enum_list["ID"];
					\CIBlockElement::SetPropertyValuesEx($request[5]['ELEMENT_ID'], $arParams["IBLOCK"], $arLoadprops);
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR5"),$request["proxy"],$request[5]['ELEMENT_ID'],$info['http_code']);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
				
			}else{
			
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR6"),$request["proxy"],$info['http_code']);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_url"),true,$request['url']);
			
			}
			
		}else{
			if(ToLower(SITE_CHARSET) != 'utf-8'){
				$response = $GLOBALS["APPLICATION"]->ConvertCharset($response, "UTF-8", SITE_CHARSET);
			}
			$saw = new \Mlife\Parser\nokogiri('<meta http-equiv="content-type" content="text/html; charset='.SITE_CHARSET.'">'.$response);
			//echo'<pre>';print_r($response);echo'</pre>';die();
			if($prop = $saw->get('img.form__captcha')->toArray()) {
				
				
				if(\COption::GetOptionString("mlife.parser","yandex_show_capcha","Y")=="Y" /*&& self::$arFilterId*/){
				$defUrl = preg_replace("/(.*)market\.([^\/]+)\/(.*)/","$2",$request[0]);
				\COption::SetOptionString("mlife.parser","yandex_show_capcha_url",$defUrl);
				$html = $saw->get('.form__inner')->toXml();
				global $APPLICATION;
				$proxer = \Mlife\Parser\ProxyTable::getProxy();
				if($request[5]['PROXY']) $proxer = array("PROXY"=>$request[5]['PROXY']);
				if($request["proxy"]) $proxer = array("PROXY"=>$request["proxy"]);
				$html = str_replace('action="/checkcaptcha"','action="'.$APPLICATION->GetCurPageParam("option=get_capcha&func=contentImage&parser=yandex&els=".$request[5]['ELEMENT_ID']."&lastproxy=".$proxer["PROXY"],array("option","parser","els","lastproxy")).'"',$html);
				$html = str_replace("\"get","\"POST",$html);

				$html = preg_replace("/(.*<root>)/is","",$html);
				$html = str_replace('<\/root>',"",$html);
				
				$img = $saw->get("img.form__captcha")->toArray();
				//print_r($response);die();
				$img = $img[0]["src"];
				
					$contentOb = new \Mlife\Parser\ContentCurl();
					//$proxy = false;
					if(\COption::GetOptionString("mlife.yandexload", "proxyotp", "N")=="Y"){
						
						if(isset($proxer["PROXY"])) $proxy = $proxer["PROXY"];
					}
					if(self::$arFilterId && \COption::GetOptionString("mlife.parser", "proxyotpel", "N")=="Y") {
						//$proxy = false;
					}else{
						if(!$proxy && \COption::GetOptionString("mlife.yandexload", "proxyotp", "N")=="Y") {
							$this->getMess("005",$profileId,array(),false);
							die();
						}
					}
					//$proxy = false;
					if($proxer["PROXY"]){
						$contentOb->n_proxy = 1;
						$contentOb->use_proxy_list = true;
						$contentOb->array_proxy = array($proxer["PROXY"]);
					}
					//print_r($contentOb->array_proxy);
					$contentOb->get($img,null,null,array("PROXY" => $proxer["PROXY"]));
					$result = $contentOb->execute();
					
					if(!$result[0] && $result[1]['http_code']=='302'){
						$contentOb = new \Mlife\Parser\ContentCurl();
						if($proxer["PROXY"]){
							$contentOb->n_proxy = 1;
							$contentOb->use_proxy_list = true;
							$contentOb->array_proxy = array($proxer["PROXY"]);
						}
						$contentOb->get($result[1]['url'],null,null,array("PROXY" => $proxer["PROXY"]));
						$result = $contentOb->execute();
					}
					
					$tmp_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp274s_yandex_capcha.jpg";
					file_put_contents($tmp_name,$result[0]);
					if(!$result[0]) {
						$tmp_name = $img;
						
						$contentOb = new \Mlife\Parser\ContentCurl();
						if($proxer["PROXY"]){
							$contentOb->n_proxy = 0;
							$contentOb->use_proxy_list = false;
							$contentOb->array_proxy = array();
						}
						$contentOb->get($result[1]['url'],null,null,array("PROXY" => $proxer["PROXY"]));
						$result = $contentOb->execute();
						if($result[0]){
							$tmp_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp274s_yandex_capcha.jpg";
							file_put_contents($tmp_name,$result[0]);
						}
						
					}
					
					$html = '<img src="'.str_replace($_SERVER["DOCUMENT_ROOT"],"",$tmp_name).'"><br><br>'.preg_replace("/(<img[^>]+>)/is","",$html);
					//$html = preg_replace("/(.*form__captcha.*src=\")(.*)(\".)/is","$1".str_replace($_SERVER["DOCUMENT_ROOT"],"",$tmp_name)."$2",$html);
					$html .= '<style>.b-link_captcha_reload, .form__audio {display:none;}</style>';
					//$APPLICATION->RestartBuffer();
					//header("Content-Type: text/html; charset=unicode");
					
					$html .= '<script type="text/javascript" src="/bitrix/js/main/jquery/jquery-1.8.3.min.js?146720357193636"></script>';
					$html .= '
					<script>
					var keyId = "";
					$(document).ready(function(){
						$.ajax({
							url: "/bitrix/tools/mlife_parser.php?key='.\COption::GetOptionString("mlife.parser", "antigate_key", "N").'",
							data: {},
							dataType : "html",
							success: function (data, textStatus) {
								if(data!="FALSE"){
									keyId = data;
									setTimeout(function(){
										
										get_capcha();
										
									},15000);
								}else{
								$("button.form__submit").click();
								}
							}
						});
					});
					
					
					function get_capcha(){
						
						$.ajax({
							url: "/bitrix/tools/mlife_parser.php?key='.\COption::GetOptionString("mlife.parser", "antigate_key", "N").'&id="+keyId,
							data: {},
							dataType : "html",
							success: function (data, textStatus) {
								if(data=="FALSE"){
									$("button.form__submit").click();
								}else if(data=="CAPCHA_NOT_READY"){
									setTimeout(function(){
										get_capcha();
									},10000);
								}else{
									$("#rep").val(data);
									$("button.form__submit").click();
								}
							}
						});
						
					}
					
					</script>
					';
					
					echo $html;die();
				
				}else{
					
					//������� ������
					if($request["proxy"]){
						\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
					}
					
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR7"),$request["proxy"],$info['http_code']);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_url"),true,$request['url']);
				
				
			}elseif($image = $saw->get(".n-gallery__item img")->toArray()){
				$imgsrc = false;
				if(isset($image[0]['src'])){
					$imgsrc = 'https:'.$image[0]['src'];
				}
				if($imgsrc){
					//print_r($imgsrc);die();
					$resa = $this->loadProductImage($imgsrc,$request[5]['ELEMENT_ID']);
					if($resa) {
					
						$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR11"),$request[5]['ELEMENT_ID']);
						$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
						
					}else{
						
						$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR12"),$request[5]['ELEMENT_ID']);
						$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
						
					}
				}
			}
			elseif($image = $saw->get(".product-card-gallery__image-container img")->toArray()){
				//print_r($image);die();
				$imgsrc = false;
				if(isset($image[0]['src'])){
					$imgsrc = 'https:'.$image[0]['src'];
				}
				if($imgsrc){
					//print_r($imgsrc);die();
					$resa = $this->loadProductImage($imgsrc,$request[5]['ELEMENT_ID']);
					if($resa) {
					
						$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR11"),$request[5]['ELEMENT_ID']);
						$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
						
					}else{
						
						$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR12"),$request[5]['ELEMENT_ID']);
						$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
						
					}
				}
			
			}else{
				
				if($request["proxy"]){
					\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
				}
				
				$arLoadprops = array();
				$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_IMG", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
				if($ar_enum_list = $db_enum_list->GetNext()){
					$arLoadprops['YA_ACTIVE_IMG'] = $ar_enum_list["ID"];
					\CIBlockElement::SetPropertyValuesEx($request[5]['ELEMENT_ID'], $arParams["IBLOCK"], $arLoadprops);
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR10"),$request[5]['ELEMENT_ID'],$request["proxy"]);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
				
			}
			
		}
		//die();
	}
	
	public function loadProductImage($image_src,$elId){
		
		\CModule::IncludeModule("iblock");
		$obOption = new \Mlife\Parser\Options\yandex\Option();
		$CLASS_FUNCTIONS = $obOption::$CLASS_FUNCTIONS;
		
		$arParams = self::$arParams;
		
		if($arParams["PARAM_EL_DETAIL_PICTURE"][1] || $arParams["PARAM_EL_PREVIEW_PICTURE"][1]){
			
			$keyImage = time();
			
			if(strpos($image_src,".png") !== false) {
				$tmp_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp".$keyImage."s_yandex.png";
			}elseif(strpos($image_src,".gif") !== false) {
				$tmp_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp".$keyImage."s_yandex.gif";
			}else{
				$tmp_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp".$keyImage."s_yandex.jpg";
			}
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_POST, 0);
			curl_setopt($ch,CURLOPT_URL,$image_src);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$im=curl_exec($ch);
			curl_close($ch);
			file_put_contents($tmp_name,$im);
			$image_src = $tmp_name;
			
			if($arParams["PARAM_EL_DETAIL_PICTURE"][1]){
				if(method_exists($CLASS_FUNCTIONS,$arParams["PARAM_EL_DETAIL_PICTURE"][1])){
					$aaa = $CLASS_FUNCTIONS::$arParams["PARAM_EL_DETAIL_PICTURE"][1]("PARAM_EL_DETAIL_PICTURE",$elId,$arParams["IBLOCK"],$image_src,$arParams["PARAM_EL_DETAIL_PICTURE"][2]);
					if($aaa){
						$arLoadField["DETAIL_PICTURE"] = $aaa;
					}
				}
			}
			if($arParams["PARAM_EL_PREVIEW_PICTURE"][1]){
				if(method_exists($CLASS_FUNCTIONS,$arParams["PARAM_EL_PREVIEW_PICTURE"][1])){
					$aaa = $CLASS_FUNCTIONS::$arParams["PARAM_EL_PREVIEW_PICTURE"][1]("PARAM_EL_PREVIEW_PICTURE",$elId,$arParams["IBLOCK"],$image_src,$arParams["PARAM_EL_PREVIEW_PICTURE"][2]);
					if($aaa){
						$arLoadField["PREVIEW_PICTURE"] = $aaa;
					}
				}
			}
			
			if(count($arLoadField)>0){
				$el = new \CIBlockElement;
				$res = $el->Update($elId, $arLoadField);
				
				$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_IMG", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
				if($ar_enum_list = $db_enum_list->GetNext()){
					$arLoadprops['YA_ACTIVE_IMG'] = $ar_enum_list["ID"];
					\CIBlockElement::SetPropertyValuesEx($elId, $arParams["IBLOCK"], $arLoadprops);
				}
				if(!$res) {
					return false;
				}
			}
			
			unlink($image_src);
		
		}
		
		return true;
		
	}
	
	public function loadProductHar($propValue,$groupValue,$elementId){
		
		\CModule::IncludeModule("iblock");
		
		$obOption = new \Mlife\Parser\Options\yandex\Option();
		$CLASS_FUNCTIONS = $obOption::$CLASS_FUNCTIONS;
		
		$arParams = self::$arParams;
		$arParamsTitle = array();
		foreach($arParams as $key=>$val){
			$arParamsTitle[$key] = $val[0];
		}
		//echo'<pre>';print_r($arParamsTitle);echo'</pre>';
		$arLoadprops = array();
		foreach($propValue as $groupId=>$propGroup){
			foreach($propGroup as $prop){
				if($prop['name']){
					$key = array_search(trim($prop['name']),$arParamsTitle);
					$keyAr = array_keys($arParamsTitle,trim($prop['name']));
					if(count($keyAr)==0 && $key) $keyAr[] = $key;
					
					if($key){
						foreach($keyAr as $key){
							
							$opt = $arParams[$key];
							if($opt[1]){
								if(substr($key,0,9)=="PARAM_EL_"){
									$paramId = substr($key,9,60);
								}elseif(substr($key,0,6)=="PARAM_"){
									$propId = substr($key,6,60);
									//$opt[1] - �������
									//$opt[2] - ��������� ��� �������
									//����� ������� ��������� ������ � ���������� ������� �������
									if(method_exists($CLASS_FUNCTIONS,$opt[1])){
										$aaa = $CLASS_FUNCTIONS::$opt[1]($propId,$elementId,$arParams["IBLOCK"],$prop['val'],$opt[2]);
										if($aaa) $arLoadprops[$propId] = $aaa;
									}
								}
							}
							
						}
					}
				}
			}
		}
		
		//echo'<pre>';print_r($arLoadprops);echo'</pre>';
		
		$key = array_search("TABLE",$arParamsTitle);
		if($key){
			$html = '<table class="mlifeDescr">';
			if(count($propValue)>0){
				foreach($propValue as $keygroup=>$props) {
					$html .= '<tr><th class="group" colspan="2">'.$groupValue[$keygroup].'</th></tr>';
					foreach($props as $prop) {
						$html .= '<tr><td class="prop_title">'.$prop['name'].'</td><td class="prop_value">'.$prop['val'].'</td></tr>';
					}
				}
			}
			$html .= "</table>";
			$opt = $arParams[$key];
			if(substr($key,0,9)=="PARAM_EL_"){
			
			}elseif(substr($key,0,6)=="PARAM_"){
				
				$propId = substr($key,6,60);
				if(method_exists($CLASS_FUNCTIONS,$opt[1])){
					$aaa = $CLASS_FUNCTIONS::$opt[1]($propId,$elementId,$arParams["IBLOCK"],$html,$opt[2]);
					if($aaa) $arLoadprops[$propId] = $aaa;
				}
				
			}
			
		}
		
		$key = array_search("DATA",$arParamsTitle);
		if($key){
			$opt = $arParams[$key];
			if(substr($key,0,9)=="PARAM_EL_"){
			
			}elseif(substr($key,0,6)=="PARAM_"){
				$propId = substr($key,6,60);
				//print_r($propId);echo'-test';
				if(method_exists($CLASS_FUNCTIONS,$opt[1])){
					//print_r($opt[1]);echo'-exists';
					$aaa = $CLASS_FUNCTIONS::$opt[1]($propId,$elementId,$arParams["IBLOCK"],array($propValue,$groupValue),$opt[2]);
					if($aaa) $arLoadprops[$propId] = $aaa;
				}
			}
		}
		
		\CIBlockElement::SetPropertyValuesEx($elementId, $arParams["IBLOCK"], $arLoadprops);
		
		$arLoadprops = array();
		$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_HAR", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
		if($ar_enum_list = $db_enum_list->GetNext()){
			$arLoadprops['YA_ACTIVE_HAR'] = $ar_enum_list["ID"];
			\CIBlockElement::SetPropertyValuesEx($elementId, $arParams["IBLOCK"], $arLoadprops);
		}
		//echo'<pre>';print_r($arLoadprops);echo'</pre>';
		//die();
		
		return true;
	}
	
	public function getCount($profileId){
		
		$arParams = self::getProfileParams($profileId);
		if(!$arParams) {
			$this->getMess("003",$profileId,array(),false);
		}else{
			\CModule::IncludeModule("iblock");
			$arFilter = array();
			$arFilter["IBLOCK_ID"] = $arParams["IBLOCK"];
			if(intval($arParams["CATEGORY"])>0) {
				$arFilter["SECTION_ID"] = $arParams["CATEGORY"];
				$arFilter["INCLUDE_SUBSECTIONS"] = "Y";
			}
			$arFilter[] = array("LOGIC"=>"AND", array("!PROPERTY_YANDEX_ID" => false), array("!PROPERTY_YANDEX_ID" => 'no'));
			if($arParams["PARAM_EL_DETAIL_PICTURE"][1] || $arParams["PARAM_EL_PREVIEW_PICTURE"][1]){
				$arFilter[] = array("LOGIC"=>"OR",array("!PROPERTY_YA_ACTIVE_HAR_VALUE" => "Y"), array("!PROPERTY_YA_ACTIVE_IMG_VALUE" => "Y"));
			}else{
				$arFilter[] = array("!PROPERTY_YA_ACTIVE_HAR_VALUE" => "Y");
			}
			if(self::$arFilterId) $arFilter['ID'] = self::$arFilterId;
			$res = \CIBlockElement::GetList(array(),$arFilter,array(),false,array());
			return $res;
		}
		
		return 0;
		
	}
	
	public function getMess($messCode,$profileId,$option=array(),$nextUrl=true,$url=false){
		
		$agent = false;
		
		$mess = false;
		
		if($messCode=="001"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR1");
		}elseif($messCode=="002"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR2");
		}elseif($messCode=="003"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR3");
		}elseif($messCode=="005"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR005");
		}
		if(isset($option["mess"])){
			$mess = $option["mess"];
		}
		
		if($url) $mess .= " URL=".$url;
		if($mess) \Mlife\Parser\LogerTable::add(array("PARSER"=>"yandex","PROFILE"=>$profileId,"TEXT"=>$mess));
		
		if(!$agent){
			echo $mess."<br/>";
		}
		
		if(!$agent && $nextUrl){
			$nextUrl = '/bitrix/admin/mlife_parser_start.php?lang='.LANG.'&parser='.self::$parserId."&ID=".$profileId."&option=".$option["option"];
			if(self::$arFilterId) $nextUrl .= "&elid=".self::$arFilterId;
			if(trim($_REQUEST["returnUrl"])) $nextUrl .= "&returnUrl=".trim(urlencode($_REQUEST["returnUrl"]));
			echo '<script>
				setTimeout( \'location="'.$nextUrl.'";\', 500 );
			</script>';
			echo '<br/><a href="'.$nextUrl.'">'.Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_REDIRECTTEXT").'</a>';
			if(trim($_REQUEST["returnUrl"])){
			$startUrl = trim($_REQUEST["returnUrl"]);
			}else{
			$startUrl = '/bitrix/admin/mlife_parser_profile.php?lang='.LANG.'&parser='.self::$parserId."&ID=".$profileId;
			}
			echo '<br/><br/><br/><a href="'.$startUrl.'">'.Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_END").'</a>';
		}elseif(!$agent){
			if(trim(urldecode($_REQUEST["returnUrl"]))) {
				$nextUrl = trim($_REQUEST["returnUrl"]);
				echo '<script>
					setTimeout( \'location="'.$nextUrl.'";\', 500 );
				</script>';
				echo '<br/><a href="'.$nextUrl.'">'.Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_REDIRECTTEXT").'</a>';
			}
		}
		
	}
	
}