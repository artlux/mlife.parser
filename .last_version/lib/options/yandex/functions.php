<?
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser\Options\yandex;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Functions extends \Mlife\Parser\Custom\YandexFunc {
	
	public function setNumericDoubleVal ($propId,$elId,$ibId,$data,$userparam){
		
		$regeg = explode("|||",$userparam);
		if(count($regeg)==2) {
			$data = preg_replace("/".$regeg[0]."/is",$regeg[1],$data);
		}
		$data = doubleval($data);
		if($data){
			return $data;
		}else{
			return false;
		}
		
	}
	
	public function setNumericStrposText ($propId,$elId,$ibId,$data,$userparam){
		
		$end = 1;
		$regeg = explode("|||",$userparam);
		if(strpos($data,$regeg[0])!==false) $end = $regeg[1];
		$data = preg_replace("/([0-9]+)(.*)/is","$1",$data);
		$data = intval($data) * $end;
		if($data){
			return $data;
		}else{
			return false;
		}
		
	}
	
	public function setCheckBoxStrpos ($propId,$elId,$ibId,$data,$userparam) {
		
		$params = explode("|||",$userparam);
		$ibCur = $ibId;
		
		$propid = $propId;
		
		$str = strlen($data);
		$val = '';
		if($str>0) {
			if(count($params)==2){
				if(strpos($data,$params[1])!==false){
					$val = $params[0];
				}else{
					$val = "";
				}
			}elseif(count($params)==1){
				$val = $params[0];
			}else{
				$val = "Y";
			}
		}
		if($val) {
			
			$db_enum_list = \CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$ibCur, "PROPERTY_ID" => $propid, "VALUE"=>$val));
			if($ar_enum_list = $db_enum_list->GetNext())
			{
				return $ar_enum_list["ID"];
			}
			
		}
		return false;
		
	}
	
}