<?
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser\Options\universal;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Parser extends \Mlife\Parser\Parser {
	
	public function startParser($profileId){
		
		self::$profileId = $profileId;
		
		if(intval($_REQUEST["elid"]) > 0) {
			self::$arFilterId = intval($_REQUEST["elid"]);
		}
		
		if(!$_REQUEST["option"] || $_REQUEST["option"]=="get_count"){
			
			$count = self::getCount($profileId);
			
			if($count>0) {
				
				$this->getMess(false,$profileId,array("mess"=>sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR4"),$count),"option"=>"get_url"),true);
			
			}else{
				
				$this->getMess("001",$profileId,array(),false);
			
			}
			
		}elseif($_REQUEST["option"]=="get_url"){
			
			$urlList = self::getUrl($profileId);
			
			if(!$urlList) $this->getMess("002",$profileId,array(),false);
			
			foreach($urlList as $link){
			
				$contentOb = new \Mlife\Parser\ContentCurl();
					
				$proxy = false;
				
				if(\COption::GetOptionString("mlife.parser", "proxyotp", "N")=="Y"){
				
					$proxer = \Mlife\Parser\ProxyTable::getList(
						array(
							'select' => array("PROXY"),
							'order' => array("ID"=>"DESC"),
							'limit' => 1
						)
					)->Fetch();
					
					if(isset($proxer["PROXY"])) $proxy = $proxer["PROXY"];
				
				}
				
				if(self::$arFilterId && \COption::GetOptionString("mlife.parser", "proxyotpel", "N")=="Y") {
					$proxy = false;
				}else{
					if(!$proxy && \COption::GetOptionString("mlife.parser", "proxyotp", "N")=="Y") {
						$this->getMess("005",$profileId,array(),false);
						die();
					}
				}
				
				if($proxy){
				
					$contentOb->n_proxy = 1;
					$contentOb->use_proxy_list = true;
					$contentOb->array_proxy = array($proxy);
				
				}
				
				$contentOb->get($link["URL"],null,null,$link);
				$result = $contentOb->execute();
				$func = $link["FUNC_PARSE"]; //колбек функция для обработки результата
				call_user_func_array(array($this,$func),$result);
				
				break;
			
			}
			
		}elseif($_REQUEST["option"]=="set_url"){
			
			$arParams = self::getProfileParams($profileId);
			
			if($arParams["PARAM_PARSE_SEL5"] && strpos($arParams["PARAM_PARSE_SEL5"],"http")!==false){
				$url = $arParams["PARAM_PARSE_SEL5"];
				
				$contentOb = new \Mlife\Parser\ContentCurl();
				
				$proxy = false;
					
				if(\COption::GetOptionString("mlife.parser", "proxyotp", "N")=="Y"){
				
					$proxer = \Mlife\Parser\ProxyTable::getList(
						array(
							'select' => array("PROXY"),
							'order' => array("ID"=>"DESC"),
							'limit' => 1
						)
					)->Fetch();
					
					if(isset($proxer["PROXY"])) $proxy = $proxer["PROXY"];
				
				}
				
				if(self::$arFilterId && \COption::GetOptionString("mlife.parser", "proxyotpel", "N")=="Y") {
					$proxy = false;
				}else{
					if(!$proxy && \COption::GetOptionString("mlife.parser", "proxyotp", "N")=="Y") {
						$this->getMess("005",$profileId,array(),false);
						die();
					}
				}
				
				if($proxy){
				
					$contentOb->n_proxy = 1;
					$contentOb->use_proxy_list = true;
					$contentOb->array_proxy = array($proxy);
				
				}
				
				$link = array(
					"URL"=>$url,
					"FUNC_PARSE"=>"setLinks"
				);
				
				$contentOb->get($link["URL"],null,null,$link);
				$result = $contentOb->execute();
				$func = $link["FUNC_PARSE"]; //колбек функция для обработки результата
				
				call_user_func_array(array($this,$func),$result);
				
			}else{
				$this->getMess("006",$profileId,array(),false);
			}
			
			
		}
		
	}
	
	//характеристики
	public function contentHar($response, $info, $request){
		
		$arParams = self::$arParams;
		$profileId = self::$profileId;
		
		$convert = false;
		if(ToLower(SITE_CHARSET) != ToLower($arParams["PARAM_PARSE_SEL4"])) $convert = ToLower($arParams["PARAM_PARSE_SEL4"]);
		$curentCharset = ToLower(SITE_CHARSET);
		if($convert) $curentCharset = $convert;
		
		if($info['http_code']!==200)
		{
			//удалить прокси
			if($request["proxy"]){
				\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
			}
			
			$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR6"),$request["proxy"],$info['http_code']);
			$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_url"),true,$request['url']);
			
		}else{
			
			$saw = new \Mlife\Parser\nokogiri('<meta http-equiv="content-type" content="text/html; charset='.$curentCharset.'">'.$response);
			$returnParams = array();
			
			if($arParams["PARAM_PARSE_SEL2"] || $arParams["PARAM_PARSE_SEL3"]){
				
				if($arParams["PARAM_PARSE_SEL2"]){
					if($textNews = $saw->get($arParams["PARAM_PARSE_SEL2"])) {
						$returnParams["TEXT"] = preg_replace("/.*<root>(.*)<\/root>.*/is","$1",$textNews->toHtml());
						if($convert) {
							$returnParams["TEXT"] = $GLOBALS["APPLICATION"]->ConvertCharset($returnParams["TEXT"], ToUpper($convert), SITE_CHARSET);
						}
					}
				}
				
				if($arParams["PARAM_PARSE_SEL3"]){
					if($imageNews = $saw->get($arParams["PARAM_PARSE_SEL3"])->toArray()) {
						$returnParams["IMAGE"] = $imageNews[0]["src"];
					}
				}
				
			}else{
				
				$this->getMess("006",$profileId,array(),false);
				
			}
			
			if(count($returnParams)>0){
				
				$resa = $this->loadProductHar($returnParams,$request[5]['ELEMENT_ID']);
				
				if($resa) {

					$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR8"),$request[5]['ELEMENT_ID']);
					$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
					
				}else{
					
					\CModule::IncludeModule('iblock');
				
					$arLoadprops = array();
					$db_enum_list = \CIBlockProperty::GetPropertyEnum("UNIVERSAL_UP", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
					if($ar_enum_list = $db_enum_list->GetNext()){
						$arLoadprops['UNIVERSAL_UP'] = $ar_enum_list["ID"];
						\CIBlockElement::SetPropertyValuesEx($request[5]['ELEMENT_ID'], $arParams["IBLOCK"], $arLoadprops);
					}
					
					$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR9"),$request[5]['ELEMENT_ID']);
					$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
					
				}
				
			}else{
				
				if($request["proxy"]){
					\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR15"),$request["proxy"]);
				$this->getMess(false,$profileId,array("mess"=>$mess),true,$request['url']);
				
			}
			
		}
		
	}
	
	//установка характеристик
	public function loadProductHar($reqContent,$elementId){
		
		\CModule::IncludeModule("iblock");
		
		$obOption = new \Mlife\Parser\Options\universal\Option();
		$CLASS_FUNCTIONS = $obOption::$CLASS_FUNCTIONS;
		
		$arParams = self::$arParams;
		$arParamsTitle = array();
		foreach($arParams as $key=>$val){
			$arParamsTitle[$key] = $val[0];
		}
		
		$arLoadprops = array();
		$arLoadFields = array();
		foreach($reqContent as $keyP=>$val) {
			
			$key = array_search($keyP,$arParamsTitle);
			$keyAr = array_keys($arParamsTitle,$keyP);
			if(count($keyAr)==0 && $key) $keyAr[] = $key;
			
			if($key){
				
				foreach($keyAr as $key){
					
					$opt = $arParams[$key];
					if($opt[1]){
					
						if(substr($key,0,12)=="PARAM_PARSE_"){
							
						}elseif(substr($key,0,9)=="PARAM_EL_"){
							$paramId = substr($key,9,60);
							$aaa = $CLASS_FUNCTIONS::$opt[1]($paramId,$elementId,$arParams["IBLOCK"],$val,$opt[2]);
							if($aaa) {
								$arLoadFields[$paramId] = $aaa;
								if($paramId=="DETAIL_TEXT" || $paramId=="PREVIEW_TEXT") {
									$arLoadFields[$paramId."_TYPE"] = "html";
								}
							}
							
						}elseif(substr($key,0,6)=="PARAM_"){
							$propId = substr($key,6,60);
							//$opt[1] - функция
							//$opt[2] - параметры для функции
							//вызов функции обработки данных и подготовки массива свойств
							if(method_exists($CLASS_FUNCTIONS,$opt[1])){
								$aaa = $CLASS_FUNCTIONS::$opt[1]($propId,$elementId,$arParams["IBLOCK"],$val,$opt[2]);
								if($aaa) $arLoadprops[$propId] = $aaa;
							}
						}
					
					}
					
				}
				
			}
			
		}
		
	//	print_r($arLoadFields);die();
		
		if(count($arLoadFields)>0){
			
			$el = new \CIBlockElement;
			$res = $el->Update($elementId, $arLoadFields);
			if(!$res) {
				return false;
			}
		}
		
		\CIBlockElement::SetPropertyValuesEx($elementId, $arParams["IBLOCK"], $arLoadprops);
		
		$arLoadprops = array();
		$db_enum_list = \CIBlockProperty::GetPropertyEnum("UNIVERSAL_UP", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
		if($ar_enum_list = $db_enum_list->GetNext()){
			$arLoadprops['UNIVERSAL_UP'] = $ar_enum_list["ID"];
			\CIBlockElement::SetPropertyValuesEx($elementId, $arParams["IBLOCK"], $arLoadprops);
		}
		
		return true;
		
	}
	
	public function setLinks($response, $info, $request){
		
		$arParams = self::$arParams;
		$profileId = self::$profileId;
		
		$convert = false;
		if(ToLower(SITE_CHARSET) != ToLower($arParams["PARAM_PARSE_SEL4"])) $convert = ToLower($arParams["PARAM_PARSE_SEL4"]);
		
		$curentCharset = ToLower(SITE_CHARSET);
		if($convert) $curentCharset = $convert;
		
		if($info['http_code']!==200)
		{
			//удалить прокси
			if($request["proxy"]){
				\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
			}
			
			$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR6"),$request["proxy"],$info['http_code']);
			$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"set_url"),true,$request['url']);
			
		}else{
			
			$saw = new \Mlife\Parser\nokogiri('<meta http-equiv="content-type" content="text/html; charset='.$curentCharset.'">'.$response);
			
			if($arParams["PARAM_PARSE_SEL1"]){
				
				if($prop = $saw->get($arParams["PARAM_PARSE_SEL1"])->toArray()) {
					
					\CModule::IncludeModule("iblock");
					
					foreach($prop as $linkContent){
						
						$name = $linkContent["#text"];
						if($convert) {
							$name = $GLOBALS["APPLICATION"]->ConvertCharset($name, ToUpper($convert), SITE_CHARSET);
						}
						$domain = preg_replace("/^(.*:\/\/.*)\/.*/is","$1",$request["url"]);
						
						if(strpos($linkContent["href"],'http')!==false) $domain = "";
						$link = $domain.trim($linkContent["href"]);
						
						
						$res = \CIBlockElement::GetList(array(),
							array(
							"IBLOCK_ID"=>$arParams["IBLOCK"],
							"PROPERTY_PARSER_LINK"=>$link
							),
							array(),false,array()
						);
						if($res==0) {
							
							$el = new \CIBlockElement;
							$arLoadField = array(
								"ACTIVE" => "N",
								"NAME" => $name,
								"IBLOCK_ID" => $arParams["IBLOCK"],
								"PROPERTY_VALUES" => array(
									"PARSER_LINK" => $link
								),
							);
							$resEl = $el->Add($arLoadField);
							if($resEl>0) {
								$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR13"),$resEl);
								$this->getMess(false,$profileId,array("mess"=>$mess),false);
							}else{
								//ошибка $resEl->LAST_ERROR
								$this->getMess(false,$profileId,array("mess"=>$resEl->LAST_ERROR),false);
							}
							
						}else{
							$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR14"),$link);
							$this->getMess(false,$profileId,array("mess"=>$mess),false);
						}
						
					}

					$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR16");
					$this->getMess(false,$profileId,array("mess"=>$mess, "option"=>"get_count"),true,$request['url']);
					
				}else{
					
					if($request["proxy"]){
						\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
					}
					
					$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR15"),$request["proxy"]);
					$this->getMess(false,$profileId,array("mess"=>$mess),true,$request['url']);
					
				}
				
			}else{
				$this->getMess("006",$profileId,array(),false);
			}
			
		}
		
	}
	
	public function getUrl($profileId){
		
		$arParams = self::getProfileParams($profileId);
		if(!$arParams) {
			$this->getMess("003",$profileId,array(),false);
		}else{
			\CModule::IncludeModule("iblock");
			$arFilter = array();
			$arFilter["IBLOCK_ID"] = $arParams["IBLOCK"];
			if(intval($arParams["CATEGORY"])>0) {
				$arFilter["SECTION_ID"] = $arParams["CATEGORY"];
				$arFilter["INCLUDE_SUBSECTIONS"] = "Y";
			}
			$arFilter[] = array("!PROPERTY_UNIVERSAL_UP_VALUE" => "Y", "!PARSER_LINK" => false);
			if(self::$arFilterId) $arFilter['ID'] = self::$arFilterId;
			$arLinks = array();
			$res = \CIBlockElement::GetList(array("rand"=>"asc"),$arFilter,false,Array("nTopCount"=>self::$limitLink),array("ID","PROPERTY_PARSER_LINK"));
			while($ar = $res->GetNext(false,false)){
				$arLinks[] = array("URL"=>$ar["PROPERTY_PARSER_LINK_VALUE"],"ELEMENT_ID"=>$ar["ID"],
				"FUNC_PARSE"=>"contentHar");
			}
			if(count($arLinks)==0) return false;
			return $arLinks;
		}
		
	}
	
	public function getCount($profileId){
		
		$arParams = self::getProfileParams($profileId);
		if(!$arParams) {
			$this->getMess("003",$profileId,array(),false);
		}else{
			\CModule::IncludeModule("iblock");
			$arFilter = array();
			$arFilter["IBLOCK_ID"] = $arParams["IBLOCK"];
			if(intval($arParams["CATEGORY"])>0) {
				$arFilter["SECTION_ID"] = $arParams["CATEGORY"];
				$arFilter["INCLUDE_SUBSECTIONS"] = "Y";
			}
			$arFilter[] = array("LOGIC"=>"AND", array("!PROPERTY_PARSER_LINK" => false), array("!PROPERTY_UNIVERSAL_UP_VALUE" => 'Y'));
			if(self::$arFilterId) $arFilter['ID'] = self::$arFilterId;
			$res = \CIBlockElement::GetList(array(),$arFilter,array(),false,array());
			return $res;
		}
		
		return 0;
		
	}
	
	
	
	public function getMess($messCode,$profileId,$option=array(),$nextUrl=true,$url=false){
		
		$agent = false;
		
		$mess = false;
		
		if($messCode=="001"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR1");
		}elseif($messCode=="002"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR2");
		}elseif($messCode=="003"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR3");
		}elseif($messCode=="005"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR005");
		}elseif($messCode=="006"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_ERROR006");
		}
		if(isset($option["mess"])){
			$mess = $option["mess"];
		}
		
		if($url) $mess .= " URL=".$url;
		if($mess) \Mlife\Parser\LogerTable::add(array("PARSER"=>"universal","PROFILE"=>$profileId,"TEXT"=>$mess));
		
		if(!$agent){
			echo $mess."<br/>";
		}
		
		if(!$agent && $nextUrl){
			$nextUrl = '/bitrix/admin/mlife_parser_start.php?lang='.LANG.'&parser='.self::$parserId."&ID=".$profileId."&option=".$option["option"];
			if(self::$arFilterId) $nextUrl .= "&elid=".self::$arFilterId;
			if(trim($_REQUEST["returnUrl"])) $nextUrl .= "&returnUrl=".trim(urlencode($_REQUEST["returnUrl"]));
			echo '<script>
				setTimeout( \'location="'.$nextUrl.'";\', 2000 );
			</script>';
			echo '<br/><a href="'.$nextUrl.'">'.Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_REDIRECTTEXT").'</a>';
			if(trim($_REQUEST["returnUrl"])){
			$startUrl = trim($_REQUEST["returnUrl"]);
			}else{
			$startUrl = '/bitrix/admin/mlife_parser_profile.php?lang='.LANG.'&parser='.self::$parserId."&ID=".$profileId;
			}
			echo '<br/><br/><br/><a href="'.$startUrl.'">'.Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_END").'</a>';
		}elseif(!$agent){
			if(trim(urldecode($_REQUEST["returnUrl"]))) {
				$nextUrl = trim($_REQUEST["returnUrl"]);
				echo '<script>
					setTimeout( \'location="'.$nextUrl.'";\', 4000 );
				</script>';
				echo '<br/><a href="'.$nextUrl.'">'.Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_PARSER_REDIRECTTEXT").'</a>';
			}
		}
		
	}
	
}