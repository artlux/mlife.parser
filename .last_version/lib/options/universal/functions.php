<?
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser\Options\universal;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Functions extends \Mlife\Parser\Custom\UniversalFunc {

	public function setHtmlDetail($propId,$elId,$ibId,$data,$userparam){
		
		$data = html_entity_decode($data);
		
		$data = preg_replace('/<a href=".+?">(.*?)<\/a>/is',"$1",$data);
		$data = preg_replace('/<img[^>]+>/is',"",$data);
		
		return $data;
		
	}
	
	public function setHtmlPreviewTruncate($propId,$elId,$ibId,$data,$userparam){
		
		$data = html_entity_decode($data);
		
		$data = preg_replace('/<a href=".+?">(.*?)<\/a>/is',"$1",$data);
		$data = preg_replace('/<img[^>]+>/is',"",$data);
		
		$data = strip_tags($data);
		if(intval($userparam)>0) $data = TruncateText($data,intval($userparam));
		
		return $data;
		
	}
	
}