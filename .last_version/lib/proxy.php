<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class ProxyTable extends Entity\DataManager
{
	public static function getFilePath()
	{
		return __FILE__;
	}
	
	public static function getTableName()
	{
		return 'mlife_parser_proxy';
	}
	
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('MLIFE_PARSER_PROXY_ENTITY_ID_FIELD'),
			),
			'PROXY' => array(
				'data_type' => 'string',
				'required' => true,
				'title' => Loc::getMessage('MLIFE_PARSER_PROXY_ENTITY_PROXY_FIELD'),
			),
		);
	}
	
	public function deleteByProxy($proxy){
		
		if(!$proxy) return;
		
		$entity = \Mlife\Parser\ProxyTable::getEntity();
		$result = new Entity\Result();
		
		//удалить файл с кукисами для данного прокси
		$user_cookie_file = $_SERVER['DOCUMENT_ROOT'].'/upload/cookies_'.$proxy.'.txt';
		unlink($user_cookie_file);
		
		// delete
		$connection = \Bitrix\Main\Application::getConnection();
		$helper = $connection->getSqlHelper();

		$tableName = $entity->getDBTableName();

		$where = 'PROXY="'.$proxy.'"';

		$sql = "DELETE FROM ".$tableName." WHERE ".$where;
		
		$connection->queryExecute($sql);

		return $result;
		
	}
	
	public static function getProxy(){
		
		$proxer = \Mlife\Parser\ProxyTable::getList(
			array(
				'select' => array("PROXY"),
				'order' => array("ID"=>"DESC"),
				'limit' => 20
			)
		);
		$proxyAr = array();
		while($dtpr = $proxer->fetch()){
			$proxyAr[] = $dtpr;
		}
		$cnt = count($proxyAr)-1;
		$proxer = $proxyAr[rand(0,$cnt)];
		
		return $proxer;
		
	}

}