<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Handlers {

	function OnAdminContextMenuShow(&$items) {
		if($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/iblock_element_edit.php" && $_REQUEST['ID'] && $_REQUEST['IBLOCK_ID'])
		{
			$arFilter = array("LOGIC"=>"OR");
			if(intval($_REQUEST['find_section_section'])>0){
				$arFilter[] = array("IBLOCK"=>intval($_REQUEST['IBLOCK_ID']), "CATEGORY" => intval($_REQUEST['find_section_section']));
				$arFilter[] = array("IBLOCK"=>intval($_REQUEST['IBLOCK_ID']), "CATEGORY" => false);
			}else{
				$arFilter[] = array("IBLOCK"=>intval($_REQUEST['IBLOCK_ID']), "CATEGORY" => false);
			}
			$res = \Mlife\Parser\Profile\YandexTable::getList(array(
				'select' => array("*"),
				'filter' => $arFilter
			));
			if($ar = $res->Fetch()){
				$items[] = array("TEXT"=>Loc::getMessage("MLIFE_PARSER_HANDLERS_TEXT1").$ar["ID"], 
				"ICON"=>"", 
				"LINK_PARAM"=>"id=\"mlife_parser_start".$ar["ID"]."\"", 
				"TITLE"=>Loc::getMessage("MLIFE_PARSER_HANDLERS_TEXT1").$ar["ID"], "LINK"=>"/bitrix/admin/mlife_parser_start.php?lang=".LANG."&
				parser=yandex&ID=".$ar["ID"]."&elid=".intval($_REQUEST['ID'])."&returnUrl=".urlencode($GLOBALS["APPLICATION"]->GetCurPageParam()));
			}
			
			$res = \Mlife\Parser\Profile\UniversalTable::getList(array(
				'select' => array("*"),
				'filter' => $arFilter
			));
			if($ar = $res->Fetch()){
				$items[] = array("TEXT"=>Loc::getMessage("MLIFE_PARSER_HANDLERS_TEXT2").$ar["ID"], 
				"ICON"=>"", 
				"LINK_PARAM"=>"id=\"mlife_parser_start".$ar["ID"]."\"", 
				"TITLE"=>Loc::getMessage("MLIFE_PARSER_HANDLERS_TEXT1").$ar["ID"], "LINK"=>"/bitrix/admin/mlife_parser_start.php?lang=".LANG."&
				parser=universal&ID=".$ar["ID"]."&elid=".intval($_REQUEST['ID'])."&returnUrl=".urlencode($GLOBALS["APPLICATION"]->GetCurPageParam()));
			}
			
		}
	}

}