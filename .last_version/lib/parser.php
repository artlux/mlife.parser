<?
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Parser {
	
	public static $classProfile = false;
	public static $parserId = false;
	public static $profileId = false;
	
	public static $arParams = false;
	
	public static $limitLink = 1;
	
	public static $arFilterId = false;
	
	public function startParser($profileId){
		
		self::$profileId = $profileId;
		
		if(!$_REQUEST["option"] || $_REQUEST["option"]=="get_count"){
		
			$count = self::getCount($profileId);
			
			if($count>0) {
				
				echo '������� ��������� ��� ���������: '.$count;
				$nextUrl = '/bitrix/admin/mlife_parser_start.php?lang='.LANG.'&parser='.self::$parserId."&ID=".$profileId."&option=get_url";
				echo '<script>
					setTimeout( \'location="'.$nextUrl.'";\', 2000 );
				</script>';
				echo '<br/><a href="'.$nextUrl.'">�������, ���� ������� �� ������������ ���������������</a>';
			}else{
				
				echo '�� ������� �������� ��� ���������';
			
			}
			
		}elseif($_REQUEST["option"]=="get_url"){
			
			$urlList = self::getUrl($profileId);
			if(!$urlList) {
				echo '������ ��������� ������';
			}else{

				foreach($urlList as $link){
					
					$contentOb = new \Mlife\Parser\ContentCurl();
					$proxer = \Mlife\Parser\ProxyTable::getProxy();
					
					$proxy = false;
					if(isset($proxer["PROXY"])) $proxy = $proxer["PROXY"];
					
					$contentOb->n_proxy = 1;
					$contentOb->use_proxy_list = true;
					$contentOb->array_proxy = array($proxy);
					
					$contentOb->get($link["URL"],null,null,$link);
					$result = $contentOb->execute();
					$func = $link["FUNC_PARSE"]; //������ ������� ��� ��������� ����������
					call_user_func_array(array($this,$func),$result);
					
				}
				
			}
			
		}
		
	}
	
	public function getUrl($profileId){
		
		$arParams = self::getProfileParams($profileId);
		if(!$arParams) {
			echo '��������� ������� �� �������';
		}else{
			
			\CModule::IncludeModule("iblock");
			$arFilter = array();
			$arFilter["IBLOCK_ID"] = $arParams["IBLOCK"];
			if(intval($arParams["CATEGORY"])>0) {
				$arFilter["SECTION_ID"] = $arParams["CATEGORY"];
				$arFilter["INCLUDE_SUBSECTIONS"] = "Y";
			}
			$arFilter["!PROPERTY_PARSER_URL"] = false;
			$arLinks = array();
			$res = \CIBlockElement::GetList(array(),$arFilter,false,Array("nTopCount"=>1),array("ID","PROPERTY_PARSER_URL"));
			while($ar = $res->GetNext(false,false)){
				$arLinks[] = array("URL"=>$ar["PROPERTY_PARSER_URL_VALUE"],"ELEMENT_ID"=>$ar["ID"],"FUNC_PARSE"=>"ContentUniversal");
			}
			
			if(empty($arLinks)) return false;
			
			return $arLinks;
			
		}
		
	}
	
	//����������� ������
	public function contentUniversal($response, $info, $request){
		
		echo'<pre>';print_r(array($response, $info, $request));echo'</pre>';
		
		if($info['http_code']!==200)
		{
			//������� ������
			if($request["proxy"]){
				\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
			}
			echo '������ ������ �������� ����� � ��� ������: '.$request["proxy"]. " (��� ".$info['http_code'].")";
			$nextUrl = '/bitrix/admin/mlife_parser_start.php?lang='.LANG.'&parser='.self::$parserId."&ID=".self::$profileId."&option=get_url";
			echo '<script>
				setTimeout( \'location="'.$nextUrl.'";\', 2000 );
			</script>';
			echo '<br/><a href="'.$nextUrl.'">�������, ���� ������� �� ������������ ���������������</a>';
			
		}else{
			
			
			
		}
		
	}
	
	public function getCount($profileId){
		
		$arParams = self::getProfileParams($profileId);
		if(!$arParams) {
			echo '��������� ������� �� �������';
		}else{
			\CModule::IncludeModule("iblock");
			$arFilter = array();
			$arFilter["IBLOCK_ID"] = $arParams["IBLOCK"];
			if(intval($arParams["CATEGORY"])>0) {
				$arFilter["SECTION_ID"] = $arParams["CATEGORY"];
				$arFilter["INCLUDE_SUBSECTIONS"] = "Y";
			}
			//$arFilter["!PROPERTY_PARSER_URL"] = false;
			$arLinks = array();
			$res = \CIBlockElement::GetList(array(),$arFilter,array(),false,array());
			return $res;
		}
		
		return 0;
		
	}
	
	public function getProfileParams($profileId=false){
		
		if(!$profileId) return false;
		
		if(!self::$arParams) {
			
			$class = self::$classProfile;
			
			$data = $class::getById($profileId)->Fetch();
			if(!$data) return false;
			
			self::$arParams = unserialize($data["PARAMS"]);
			self::$arParams["IBLOCK"] = $data["IBLOCK"];
			self::$arParams["CATEGORY"] = $data["CATEGORY"];
		
		}
		
		return self::$arParams;
		
	}
	
}