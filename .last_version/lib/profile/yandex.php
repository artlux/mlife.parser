<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser\Profile;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class YandexTable extends Entity\DataManager
{
	public static function getFilePath()
	{
		return __FILE__;
	}
	
	public static function getTableName()
	{
		return 'mlife_parser_profile_yandex';
	}
	
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('MLIFE_PARSER_YANDEX_ENTITY_ID_FIELD'),
			),
			'IBLOCK' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('MLIFE_PARSER_YANDEX_ENTITY_IBLOCK_FIELD'),
			),
			'CATEGORY' =>array(
				'data_type' => 'integer',
				'required' => false,
				'title' => Loc::getMessage('MLIFE_PARSER_YANDEX_ENTITY_CATEGORY_FIELD'),
			),
			'PARAMS' => array(
				'data_type' => 'string',
				'required' => true,
				'title' => Loc::getMessage('MLIFE_PARSER_YANDEX_ENTITY_PARAMS_FIELD'),
			),
		);
	}

}