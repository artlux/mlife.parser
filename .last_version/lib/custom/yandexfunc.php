<?
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser\Custom;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class YandexFunc extends \Mlife\Parser\Functions {

	public function SetOptionLinkEl ($propId,$elId,$ibId,$data,$userparam) {
		//echo '<pre>';print_r($data);echo'</pre>';
		//echo '<pre>';print_r($userparam);echo'</pre>';
		$iblLink = $userparam;
		
		$propid = $propId;
		
		$ibCur = $ibId;
		
		if($data) {
			$el = \CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$iblLink,"NAME"=>trim($data),false,false,array("ID")))->fetch();
			if($el['ID']){
				$ID = $el['ID'];
			}else{
				$eln = new \CIBlockElement;
				
				$params = array(
				"max_len" => "30", 
				"change_case" => "L", 
				"replace_space" => "_", 
				"replace_other" => "_", 
				"delete_repeat_replace" => "true", 
				"use_google" => "false", 
				);


				$code = \CUtil::translit($data, "ru", $params);
				
				$ID = $eln->Add(array("IBLOCK_ID"=>$iblLink,"NAME"=>trim($data),"CODE"=>$code));
				//echo '<pre>';print_r($eln->LAST_ERROR);echo'</pre>'; die();
			}
			
			if($ID){
				return $ID;
				$nnn = false;
				$db_enum_list = \CIBlockPropertyEnum::GetList(array(), Array("IBLOCK_ID"=>$ibCur, "PROPERTY_ID" => $propid, "VALUE"=>$ID));
				while($ar_enum_list = $db_enum_list->GetNext())
				{
					if($ar_enum_list["VALUE"]){
						$finData[] = array("VALUE"=>$ar_enum_list["ID"]);
						$nnn = true;
					}
					
				}
				
			}
			
		}
		return false;
		
	}
	
	public function setPropStringMultiple($propId,$elId,$ibId,$data,$userparam){
		
		$props = $data[0];
		$groups = $data[1];
		
		$finData = array();
		
		foreach($props as $group=>$ar){
			$finData[] = array("VALUE"=>($groups[$group] ? $groups[$group] : '-'), "DESCRIPTION"=>'+++');
			foreach($ar as $v){
				$finData[] = array("VALUE"=>$v['name'],"DESCRIPTION"=>$v['val']);
			}
		}
		if(count($finData)>0) return $finData;
		
		return false;
		
	}

}