<?
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class ContentCurl {

	public $use_proxy_list       =   false;
	public $use_useragent_list   =   false;
	public $array_proxy   =   array();
	
	public $requests = array();
	
	public $n_proxy              =   0;
	public $n_useragent          =   0;
	
	function load_useragent_list($input) {
		
		if(is_array($input))
        {
            $this->array_useragent = $input;
        }
        else
        {        
            $this->array_useragent = $this->load_from_file($input);
        }
		
		$this->n_useragent = count($this->array_useragent);
		 
		$this->use_useragent_list = true;
        
        return $this->n_useragent;
	}
	
	
	public function get($url, $headers = null, $options = null, $additionalOptions=null) {
        $this->requests[] = array($url, "GET", null, $headers, $options, $additionalOptions);
    }
	
	public function execute(){
		
		$val = \COption::GetOptionString("mlife.parser", "limit2", "6");
		
		if(count($this->requests)>0){
			
			foreach($this->requests as $request){
				sleep($val);
				$ch = curl_init();
				$options = $this->get_options($request);
				
				curl_setopt_array($ch, $options);
				
				$output = curl_exec($ch);
				$info = curl_getinfo($ch);

				curl_close($ch);

				$requestAr = $request;
				$requestAr["url"] = $request[0];
				$requestAr["addoptions"] = $request[5];
				$requestAr["proxy"] = false;
				if(isset($this->array_proxy[0])){
					$requestAr["proxy"] = $this->array_proxy[0];
				}
				
				$fin = array($output, $info, $requestAr);
				return $fin;
				
			}
			
		}
		
	}
	
	public function get_options($request) {

        $options = $request[4];
		$options[CURLOPT_FOLLOWLOCATION] = 1;
        $options[CURLOPT_MAXREDIRS] = 5;
        $headers = $request[3];

        // set the request URL
        $options[CURLOPT_URL] = $request[0];
		$options[CURLOPT_TIMEOUT] = \COption::GetOptionString("mlife.parser", "limit1", "15");

        // posting data w/ this request?
        if ($request[1]=="POST") {
            $options[CURLOPT_POST] = 1;
            $options[CURLOPT_POSTFIELDS] = $request[2];
        }
		$options[CURLOPT_HEADER] = 0;
        if ($headers) {
            $options[CURLOPT_HEADER] = 0;
            $options[CURLOPT_HTTPHEADER] = $headers;
        }
		
		if($this->n_proxy>0){
			if($this->n_proxy == 1) {
				$proxyKey = 0;
			}else{
				$proxyKey = mt_rand(0, $this->n_proxy-1);
			}
			$options[CURLOPT_PROXY]=$this->array_proxy[$proxyKey];
		}
		
		if($this->n_useragent > 0 && $this->use_useragent_list)
		{
			$options[CURLOPT_USERAGENT]=$this->array_useragent[ mt_rand(0, $this->n_useragent-1) ];
		}else{
			$options[CURLOPT_USERAGENT] = $_SERVER['HTTP_USER_AGENT'];
		}
		$options[CURLOPT_RETURNTRANSFER]=1;
		//
		if(!$this->array_proxy[$proxyKey]) {
			print_r($this->array_proxy[$proxyKey]);echo'-'.$this->requests[0][0].'<br/>';
			//die();
		}
		$user_cookie_file = $_SERVER['DOCUMENT_ROOT'].'/upload/cookies_'.$this->array_proxy[$proxyKey].'.txt'; 
		
		//curl_setopt($ch, CURLOPT_COOKIESESSION, 1);
		$options[CURLOPT_COOKIEJAR] =  $user_cookie_file;
		$options[CURLOPT_COOKIEFILE] = $user_cookie_file;

        return $options;
    }
	
	public function load_from_file($filename, $delim = "\n"){
        $data;
        $fp = @fopen($filename, "r");
        
        if(!$fp)
        {
            return array();
        }
        
        $data = @fread($fp, filesize($filename) );
        fclose($fp);
        
        if(strlen($data)<1)
        {
            return array();
        }
        
        $array = explode($delim, $data);
        
        if(is_array($array) && count($array)>0)
        {
            foreach($array as $k => $v)
            {
                if(strlen( trim($v) ) > 0)
                    $array[$k] = trim($v);
            }
            return $array;
        }
        else
        {
            return array();
        }
    }

}